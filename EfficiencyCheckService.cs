﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace QuerySamples
{
    class EfficiencyCheckService<T>
    {
        public static double checkEfficiency(int repNumber, IEnumerable ienumerable)
        {
            List<double> elapsedTimeList = new List<double>();
            Stopwatch stopwatch = Stopwatch.StartNew();

            for (int i = 0; i < repNumber; i++)
            {

                var enumerator = ienumerable.GetEnumerator();
                object n;
                while (enumerator.MoveNext())
                {
                    n = enumerator.Current;
                }
                elapsedTimeList.Add((double)stopwatch.Elapsed.TotalMilliseconds * 1000000);
                stopwatch.Restart();
            }

            stopwatch.Stop();
            return getMediana(elapsedTimeList);
        }

        private static double getMediana(List<double> sourceNumbers)
        {
            if (sourceNumbers == null || sourceNumbers.Count == 0)
                throw new System.Exception("Median of empty array not defined.");

            List<double> sortedPNumbers = sourceNumbers.OrderBy(x => x).ToList();

            //get the median
            int size = sortedPNumbers.Count;
            int mid = size / 2;
            double median = (size % 2 != 0) ? (double)sortedPNumbers.ElementAt(mid) : ((double)sortedPNumbers.ElementAt(mid) + (double)sortedPNumbers.ElementAt(mid - 1)) / 2;
            return median;
        }
    }
}
