lab1:
Zad3.1:
1.1:

```
#!c#

[Category("Lab1")]
[Title("Select - names and unit prices, zad 1.1")]
[Description("Return names and unit prices.")]
public void LinqIns1Zad3_1Punkt1_1() {
	List<Product> products = GetProductList();

	var productNamesAndUnitPrices =
		from prod in products
		select new { name = prod.ProductName, unitPrice = prod.UnitPrice};
	
	Console.WriteLine("Product Names and unit prices:");
	foreach (var productNameAndUnitPrices in productNamesAndUnitPrices) {
		Console.WriteLine("Name: {0}, Unit price: {1}", productNameAndUnitPrices.name, productNameAndUnitPrices.unitPrice);
	}
}
```


1.2:

```
#!c#

[Category("Lab1")]
[Title("Zad 1.2")]
[Description("Return names of available products which costs under 10 and belong to category Seafood")]
public void LinqIns1Zad3_1Punkt1_2()
{
	List<Product> products = GetProductList();

	var productNames =
		from prod in products
		where prod.UnitsInStock > 0 && prod.UnitPrice < 10 && prod.Category == "Seafood"
		select prod.ProductName;

	Console.WriteLine("Product Names:");
	foreach (var name in productNames)
	{
		Console.WriteLine("Name: {0}", name);
	}
}
```


1.3:

```
#!c#

[Category("Lab1")]
[Title("Zad 1.3")]
[Description("Return products which price is equal to Ikura price")]
public void LinqIns1Zad3_1Punkt1_3()
{
	List<Product> products = GetProductList();

	decimal IkuraPrice =
		(from prod in products
		where prod.ProductName == "Ikura"
		select (decimal)prod.UnitPrice).Single();

	var soughtProducts =
		from prod in products
		where prod.UnitPrice.Equals(IkuraPrice)
		select prod;

	Console.WriteLine("Product Names:");
	foreach (var prod in soughtProducts)
	{
		Console.WriteLine("Name: {0}", prod.ProductName);      
	}
}
```


1.4:

```
#!c#

[Category("Lab1")]
[Title("Zad 1.4")]
[Description("Return average price in every category")]
public void LinqIns1Zad3_1Punkt1_4()
{
	List<Product> products = GetProductList();

	var categories =
		from prod in products
		group prod by prod.Category into prodGroup
		select new { Category = prodGroup.Key, AveragePrice = prodGroup.Average(p => p.UnitPrice) };

	foreach (var c in categories)
	{
		Console.WriteLine("Category: {0}, AveragePrice: {1}", c.Category, c.AveragePrice);
	}
}
```


2.1:

```
#!c#

[Category("Lab1")]
[Title("Zad2.1")]
[Description("Return customers cities and names for customers with no faks")]
public void XLinqIns1Zad3_1Punkt2_1()
{
	IEnumerable<XElement> root = XDocument.Load(dataPath + "nw_customers.xml").Elements("Root");
	var query =
		from
			customer in root.Elements("Customers")
		where
			customer.Element("Fax") == null
		select new { name = customer.Element("ContactName").Value, city = customer.Element("FullAddress").Element("City").Value };
	foreach (var result in query)
		Console.WriteLine("Customer name {0}, City {1}",
						  result.name,
						  result.city);

}

```

2.2:

```
#!c#

[Category("Lab1")]
[Title("Zad2.2")]
[Description("Return names of cities along with number of associated customers in descending manner")]
public void XLinqIns1Zad3_1Punkt2_2()
{
	XDocument customers = XDocument.Load(dataPath + "nw_customers.xml");
	var directory =
		from customer in customers.Descendants("Customers")
		from city in customer.Elements("FullAddress").Elements("City")
		group customer by (string)city into cityGroup
		select new { city = cityGroup.Key, customersNumber = cityGroup.Count()};

	foreach (var d in directory)
	{
		Console.WriteLine("City: {0}, Customers number: {1}", d.city, d.customersNumber);
	}
}
```


3:

```
#!c#

[Category("Lab1")]
[Title("Zad 3")]
[Description("Return prime number from 1 to 888")]
public void LinqIns1Zad3_1Punkt3()
{
	const int start = 1;
	const int end = 888;

	var query =
		Enumerable.Range(start, end)
		.Where(number =>
			Enumerable.Range(2, (int)Math.Sqrt(number) - 1)
			.All(divisor => number % divisor != 0));

	foreach (var number in query)
	{
		Console.WriteLine(number);
	}
}

```

3.2.1:

```
#!c#

class EfficiencyCheckService<T>
    {
		public static double checkEfficiency(int repNumber, IEnumerable ienumerable)
        {
            List<long> elapsedTimeList = new List<long>();
            Stopwatch stopwatch = Stopwatch.StartNew();

            for (int i = 0; i < repNumber; i++)
            {

                var enumerator = ienumerable.GetEnumerator();
                object n;
                while (enumerator.MoveNext())
                {
                    n = enumerator.Current;
                }
                elapsedTimeList.Add(stopwatch.ElapsedMilliseconds);
                stopwatch.Restart();
            }

            stopwatch.Stop();
            return getMediana(elapsedTimeList);
        }

        private static double getMediana(List<long> sourceNumbers)
        {
            if (sourceNumbers == null || sourceNumbers.Count == 0)
                throw new System.Exception("Median of empty array not defined.");

            List<long> sortedPNumbers = sourceNumbers.OrderBy(x => x).ToList();

            //get the median
            int size = sortedPNumbers.Count;
            int mid = size / 2;
            double median = (size % 2 != 0) ? (double)sortedPNumbers.ElementAt(mid) : ((double)sortedPNumbers.ElementAt(mid) + (double)sortedPNumbers.ElementAt(mid - 1)) / 2;
            return median;
        }
    }


[Category("Lab1")]
[Title("Zad 3.2.1")]
[Description("Test EfficiencyChecker for Zad 1.2")]
public void LinqIns1Zad3_2Punkt123()
{
	List<Product> products = GetProductList();

	IEnumerable<Product> someProducts =
		from prod in products
		where prod.UnitsInStock > 0 && prod.UnitPrice < 10 && prod.Category == "Seafood"
		select prod;

	double mediana = EfficiencyCheckService<Product>.checkEfficiency(100, prod => prod.UnitsInStock > 0 && prod.UnitPrice < 10 && prod.Category == "Seafood", products);
	Console.WriteLine(mediana);
}

[Category("Lab1")]
[Title("Zad 3.2.1")]
[Description("Test EfficiencyChecker")]
public void XLinqIns1Zad3_2Punkt123()
{
	List<XElement> list = XDocument.Load(dataPath + "nw_customers.xml").Elements("Customers").ToList();

	double mediana = EfficiencyCheckService<XElement>.checkEfficiency(100, c => c.Element("FullAddress").Element("Country").Value == "Germany", list);
	Console.WriteLine(mediana);
}

```


lab2:
3.1.1

```
#!c#
[Category("Lab1")]
[Title("Zad 3.1.1")]
[Description("Find supplier who produce Ikura.")]
public void LinqToSqlIns2Zad3_1Punkt1()
{
	var q =
		from s in db.Suppliers
		where s.Products.Any(p => p.ProductName == "Ikura")
		select s;

	foreach (var sup in q)
	{
		Console.WriteLine("Supplier ID {0}, Contact name {1}", sup.SupplierID, sup.ContactName);
	}
}
```


3.1.2

```
#!c#

[Category("Lab1")]
[Title("Zad 3.1.2")]
[Description("Find customers who bought Ikura.")]
public void LinqToSqlIns2Zad3_1Punkt2()
{
	IQueryable<int> pId = 
		from p in db.Products
		where p.ProductName == "Ikura"
		select p.ProductID;

	var q =
		from c in db.Customers
		where c.Orders.Any(o => o.OrderDetails.Any(od => od.ProductID == pId.Single()))
		select c;

	foreach (var cus in q)
	{
		Console.WriteLine("Contact name {0}", cus.ContactName);
	}
}
```


3.1.3

```
#!c#

[Category("Lab1")]
[Title("Zad 3.1.3")]
[Description("Group orders by shippers, emplyees and customers")]
public void LinqToSqlIns2Zad3_1Punkt3()
{
	var q =
		from o in db.Orders
		group o by new { o.Shipper, o.Employee, o.Customer } into groupShipper
		select new { groupShipper.Key, groupShipper };

	ObjectDumper.Write(q, 1);
}
```


3.2.1

```
#!c#
[Category("Lab2")]
[Title("Zad 3.2.1")]
[Description("Return products which are available, cost less then 10 and belong to the Seafood category")]
public void LinqIns2Zad3_2Punkt1()
{
	List<Product> products = GetProductList();

	var productNames = GetProductList().Where(p => p.UnitsInStock > 0 &&
		p.UnitPrice < 10 && p.Category == "Seafood").Select(p => p.ProductName);

	var productNamesPar = GetProductList().AsParallel().Where(p => p.UnitsInStock > 0 &&
		p.UnitPrice < 10 && p.Category == "Seafood").Select(p => p.ProductName);

	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, productNames) + " ns");    // 149800 ns
	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, productNamesPar) + " ns"); // 389150 ns, to jedyny przypadek, w którym zapytanie asParallel wykonuje się wolniej
	Console.WriteLine("Product Names:");
	foreach (var name in productNames)
	{
		Console.WriteLine("Name: {0}", name);
	}
}
```

3.2.2

```
#!c#
[Category("Lab2")]
[Title("Zad 3.2.2")]
[Description("Return cheapest and the most expensive product in each category")]
public void LinqIns2Zad3_2Punkt2()
{
	List<Product> products = GetProductList();

	var categories = GetProductList().GroupBy(p => p.Category)
	   .Select(c => new {
		   Category = c.Key,
		   MinPriceProd = c.First(p => p.UnitPrice == c.Min(m => m.UnitPrice)).ProductName,
		   MaxPriceProd = c.First(p => p.UnitPrice == c.Max(m => m.UnitPrice)).ProductName
	   });

	var categoriesPar = GetProductList().AsParallel().GroupBy(p => p.Category)
	   .Select(c => new
	   {
		   Category = c.Key,
		   MinPriceProd = c.First(p => p.UnitPrice == c.Min(m => m.UnitPrice)).ProductName,
		   MaxPriceProd = c.First(p => p.UnitPrice == c.Max(m => m.UnitPrice)).ProductName
	   });

	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, categories) + " ns");    //416347950 ns
	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, categoriesPar) + " ns"); //376202500 ns
	foreach (var c in categories)
	{
		Console.WriteLine("Category: {0}, MinPriceProductName: {1}, MaxPriceProductName: {2}", c.Category, c.MinPriceProd, c.MaxPriceProd);
	}
}
```


3.2.3

```
#!c#
[Category("Lab2")]
[Title("Zad 3.2.3")]
[Description("Return price with most units in stock")]
public void LinqIns2Zad3_2Punkt3()
{
	List<Product> products = GetProductList();

	var p = products.GroupBy(prod => prod.UnitPrice).Select(c => new
	{
		ProductPrice = c.Key,
		UnitsOverall = c.Sum(prod => prod.UnitsInStock)
	});

	var pPar = products.AsParallel().GroupBy(prod => prod.UnitPrice).Select(c => new
	{
		ProductPrice = c.Key,
		UnitsOverall = c.Sum(prod => prod.UnitsInStock)
	});

	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, p) + " ns");    // 2454550 ns
	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, pPar) + " ns"); // 1917950 ns
	foreach (var price in p)
	{
		Console.WriteLine("Price: {0}, units in stock overall: {1}", price.ProductPrice, price.UnitsOverall);
	}
}
```


3.2.4

```
#!c#
[Category("Lab2")]
[Title("Zad 3.2.4")]
[Description("Return product names cheaper or less numerous")]
public void LinqIns2Zad3_2Punkt4()
{
	List<Product> products = GetProductList();

	var p = products.Select(prod => new
	{
		productName = prod.ProductName,
		counter = products.Where(c => c.UnitsInStock < prod.UnitsInStock || c.UnitPrice < prod.UnitPrice).Count(),
	});

	var pPar = products.AsParallel().Select(prod => new
	{
		productName = prod.ProductName,
		counter = products.Where(c => c.UnitsInStock < prod.UnitsInStock || c.UnitPrice < prod.UnitPrice).Count(),
	});

	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, p) + " ns");    // 2391730550 ns
	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, pPar) + " ns"); // 951653600 ns
	foreach (var item in p)
	{
		Console.WriteLine("Product name: {0}, number of products cheaper or less numerous: {1}", item.productName, item.counter);
	}
}
```


3.2.5

```
#!c#
[Category("Lab2")]
[Title("Zad 3.2.5")]
[Description("Return number of products which cost the same as given product")]
public void LinqIns2Zad3_2Punkt5()
{
	List<Product> products = GetProductList();

	var p = products.Select(prod => new
	{
		productName = prod.ProductName,
		counter = products.Where(c => c.UnitPrice == prod.UnitPrice).Count(),
	});

	var pPar = products.AsParallel().Select(prod => new
	{
		productName = prod.ProductName,
		counter = products.Where(c => c.UnitPrice == prod.UnitPrice).Count(),
	});

	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, p) + " ns");    // 1355521500 ns
	Console.WriteLine(EfficiencyCheckService<Product>.checkEfficiency(10, pPar) + " ns"); // 654305450 ns

	foreach (var item in p)
	{
		Console.WriteLine("Product name: {0}, number of products with identical price: {1}", item.productName, item.counter);
	}

}
```